﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;

namespace Rabbit.Ping.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var cf = new ConnectionFactory()
            {
                HostName = "localhost"
            };

            using (var c = cf.CreateConnection())
            {
                using (var channel = c.CreateModel())
                {
                    channel.ExchangeDeclare("ping", ExchangeType.Direct);
                    channel.QueueDeclare("ping",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    channel.QueueBind("ping", "ping", "ping");

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (sender, eventArgs) =>
                    {
                        var replyTo = eventArgs.BasicProperties.ReplyTo;
                        var correlationId = eventArgs.BasicProperties.CorrelationId;
                        Console.WriteLine($"{eventArgs.BasicProperties.CorrelationId}");
                        Console.WriteLine($"{DateTime.Now.TimeOfDay} - {Encoding.UTF8.GetString(eventArgs.Body)}");

                        channel.BasicPublish(string.Empty, routingKey:replyTo, basicProperties: new BasicProperties {CorrelationId = correlationId}, body:Encoding.UTF8.GetBytes($"Pong - {DateTime.Now.TimeOfDay}"));
                    };

                    channel.BasicConsume("ping", true, consumer);
                    Console.WriteLine("i am rpc server");
                    Console.ReadLine();
                }
            }
        }
    }
}
