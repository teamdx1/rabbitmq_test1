﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;

namespace Rabbit.Ping.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var cf = new ConnectionFactory()
            {
                HostName = "localhost"
            };

            using (var c = cf.CreateConnection())
            {
                using (var channel = c.CreateModel())
                {
                    var queue = channel.QueueDeclare();

                    var basicProps = new BasicProperties
                    {
                        ReplyTo = queue.QueueName
                    };

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (sender, eventArgs) =>
                    {
                        Console.WriteLine($"{eventArgs.BasicProperties.CorrelationId}");
                        Console.WriteLine($"{DateTime.Now.TimeOfDay} - {Encoding.UTF8.GetString(eventArgs.Body)}");
                    };

                    channel.BasicConsume(queue.QueueName, true, consumer);

                    Console.WriteLine("i am rpc client");

                    while (true)
                    {
                        Thread.Sleep(500);
                        var p = (BasicProperties)basicProps.Clone();
                        p.CorrelationId = Guid.NewGuid().ToString();
                        channel.BasicPublish("ping", "ping", p, Encoding.UTF8.GetBytes($"Ping - {DateTime.Now.TimeOfDay}"));
                    }
                }
            }
        }
    }
}
