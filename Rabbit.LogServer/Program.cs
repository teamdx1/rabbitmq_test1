﻿using System;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;

namespace Rabbit.LogServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost"
            };

            using (var conn = factory.CreateConnection())
            {
                var serverTask = Task.Factory.StartNew(() =>
                {
                    using (var channel = conn.CreateModel())
                    {
                        channel.ExchangeDeclare("logs", ExchangeType.Topic, true, false, null);
                        Console.WriteLine("Server started");
                        string result;
                        do
                        {
                            result = Console.ReadLine();
                            var date = DateTime.Now;
                            Console.WriteLine($"Sent - {date.TimeOfDay}");
                            channel.BasicPublish("logs", "logs." + result, new BasicProperties(),
                                ToBytes(date.TimeOfDay.ToString()));
                        } while (result != "quit");
                    }
                });

                Task.Factory.StartNew(() =>
                {
                    var channel = conn.CreateModel();
                    channel.QueueDeclare("l1", true, false, false, null);
                    channel.QueueBind("l1", "logs", "*.l1");
                    Console.WriteLine("l1 started");
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (sender, eventArgs) =>
                    {
                        Console.WriteLine($"L1 - {DateTime.Now.TimeOfDay} - {FromBytes(eventArgs.Body)}");
                        channel.BasicAck(eventArgs.DeliveryTag, false);
                    };
                    channel.BasicConsume("l1", false, consumer);
                });

                Task.Factory.StartNew(() =>
                {
                    var channel = conn.CreateModel();
                    channel.QueueDeclare("all", true, false, false, null);
                    channel.QueueBind("all", "logs", "*.*");
                    Console.WriteLine("all started");
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (sender, eventArgs) =>
                    {
                        Console.WriteLine($"All - {DateTime.Now.TimeOfDay} - {FromBytes(eventArgs.Body)}");
                        channel.BasicAck(eventArgs.DeliveryTag, false);
                    };
                    channel.BasicConsume("all", false, consumer);
                });

                Task.WaitAll(serverTask);
            }
        }

        private static byte[] ToBytes(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        private static string FromBytes(byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }
    }
}
